package com.example.eric.altrantest.webservice.weather

data class Forecast(val dt: String,
                    val temp: Temp? = null,
                    val pressure: Float? = null,
                    val humidity: Int? = null,
                    val weather: List<Weather>? = null,
                    val speed: Float? = null,
                    val deg: Int? = null,
                    val clouds: Int? = null,
                    val rain: Float? = null)