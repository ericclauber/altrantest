package com.example.eric.altrantest.weather.adapter

import android.content.Context
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.example.eric.altrantest.R
import com.example.eric.altrantest.base.LineItem
import com.example.eric.altrantest.webservice.weather.Forecast

/**
 * Created by eric on 21/06/18.
 */
class WeatherAdapter(val mContext: Context,
                     internal var mListener: OnClickListener,
                     var mLineItemList: MutableList<LineItem>)
    : RecyclerView.Adapter<RecyclerView.ViewHolder>() {

    var mLineItem: MutableList<LineItem>? = null


    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        val view: View? = LayoutInflater
                .from(mContext)
                .inflate(R.layout.line_item_forecast, parent, false)

        return WeatherHolder(mContext, view!!, mListener)
    }

    override fun getItemCount(): Int {

        return mLineItemList.size
    }

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {

        (holder as WeatherHolder).bindItem(position, mLineItemList.get(position))
    }

    override fun getItemViewType(position: Int): Int {

        return position
    }

    interface OnClickListener {

        fun onClickItem(forecast: Forecast)
    }
}