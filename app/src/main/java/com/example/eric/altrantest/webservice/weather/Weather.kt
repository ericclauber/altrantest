package com.example.eric.altrantest.webservice.weather

data class Weather(var id: Int) {

    var main: String = ""
    var description: String = ""
    var icon: String = ""
}