package com.example.eric.altrantest.databasehelper

import android.content.Context
import android.database.sqlite.SQLiteDatabase
import android.database.sqlite.SQLiteOpenHelper

/**
 * Created by eric on 19/06/18.
 */
class DbHelper(context: Context, queryCreateTable: String, tableNameDelete: String)
    : SQLiteOpenHelper(context, DATABASE_NAME, null, DATABASE_VERSION) {

    private val SQL_CREATE_USER = "CREATE TABLE ${queryCreateTable}"
    private val SQL_DELETE_USER = "DROP TABLE IF EXISTS ${tableNameDelete}"

    companion object {

        const val DATABASE_VERSION = 1
        const val DATABASE_NAME = "database.db"
    }

    override fun onCreate(db: SQLiteDatabase?) {

        db?.execSQL(SQL_CREATE_USER)
    }

    override fun onUpgrade(db: SQLiteDatabase?, oldVersion: Int, newVersion: Int) {

        db?.execSQL(SQL_DELETE_USER)
        onCreate(db)
    }
}