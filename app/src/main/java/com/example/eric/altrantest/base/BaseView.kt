package com.example.eric.altrantest.base

/**
 * Created by eric on 20/06/18.
 */
interface BaseView {

    enum class ProgressType {
        PROGRESS_DIALOG,
    }


    fun showProgress(type: ProgressType)
    fun hideProgress()
}