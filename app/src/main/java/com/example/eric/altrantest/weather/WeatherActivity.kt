package com.example.eric.altrantest.weather

import android.annotation.SuppressLint
import android.location.Location
import android.os.Bundle
import android.support.v7.widget.DividerItemDecoration
import android.support.v7.widget.LinearLayoutManager
import android.util.Log
import android.view.View
import com.example.eric.altrantest.R
import com.example.eric.altrantest.base.BaseActivity
import com.example.eric.altrantest.base.BaseView
import com.example.eric.altrantest.base.LineItem
import com.example.eric.altrantest.base.Utils
import com.example.eric.altrantest.weather.adapter.WeatherAdapter
import com.example.eric.altrantest.webservice.weather.Forecast
import com.example.eric.altrantest.webservice.weather.WeatherResponse
import com.google.android.gms.common.ConnectionResult
import com.google.android.gms.common.api.GoogleApiClient
import com.google.android.gms.location.FusedLocationProviderClient
import com.google.android.gms.location.LocationServices
import kotlinx.android.synthetic.main.activity_forecast.*
import kotlinx.android.synthetic.main.custom_header_bar.*
import kotlin.properties.Delegates

class WeatherActivity : BaseActivity(), WeatherView, WeatherAdapter.OnClickListener, GoogleApiClient.ConnectionCallbacks,
        GoogleApiClient.OnConnectionFailedListener  {

    var weatherPresenterImpl: WeatherPresenterImpl by Delegates.notNull()
    var mWeatherAdapter: WeatherAdapter? = null
    var mLineItemList: MutableList<LineItem> = ArrayList()
    var mUserLocation: Location? = null
    val QUANT_DAYS = 17
    var mGoogleApiClient: GoogleApiClient? = null

    private lateinit var fusedLocationClient: FusedLocationProviderClient

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_forecast)

        initUI()
    }

    fun initUI() {

        textViewTitleHeaderBar.text = getString(R.string.weather_title)
        imageViewBack.visibility = View.VISIBLE
        imageViewBack.setOnClickListener { onBackPressed() }

        if (!checkLocationPermission()) {
            requestLocationPermission()
            callConnection()
        } else {
            callConnection()
        }

        setupRecyclerView()
    }

    fun initData() {

        weatherPresenterImpl = WeatherPresenterImpl(this, this)

        if (mUserLocation != null) {
            weatherPresenterImpl.getWeather(mUserLocation?.latitude!!.toFloat(),
                    mUserLocation?.longitude!!.toFloat(), QUANT_DAYS)
        } else {

            weatherPresenterImpl.getWeather(-23.539312f, -46.799194f, QUANT_DAYS)
        }
    }

    fun setupRecyclerView() {

        mWeatherAdapter = WeatherAdapter(this, this, mLineItemList!!)

        val itemdecorator = DividerItemDecoration(this, DividerItemDecoration.VERTICAL)
        val llm = LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false)
        recyclerView.setHasFixedSize(true)
        recyclerView.layoutManager = llm
        recyclerView.addItemDecoration(itemdecorator)

        recyclerView.adapter = mWeatherAdapter
    }

    @SuppressLint("MissingPermission")
    fun callConnection() {

        mGoogleApiClient = GoogleApiClient.Builder(this)
                .addOnConnectionFailedListener(this)
                .addConnectionCallbacks(this)
                .addApi(LocationServices.API)
                .build()

        mGoogleApiClient!!.connect()
    }

    override fun getWeatherSuccess(weatherResponse: WeatherResponse) {

        textViewCity.text = weatherResponse.city.name
        textViewTime.text = Utils.timestampToDate(weatherResponse.list.get(0).dt.toLong())
        textViewWheather.text = weatherResponse.list.get(0).weather?.get(0)?.description?.capitalize()
        textViewTemperature.text = Utils.convertKelvinToCelcius(weatherResponse.list.get(0).temp?.day!!)

        for (i in 1..weatherResponse.list.size - 1) {

            val forecast = weatherResponse.list.get(i) as Forecast
            val lineItem = LineItem(forecast, false, 0, 0)
            mLineItemList.add(lineItem)
        }

        mWeatherAdapter?.notifyDataSetChanged()
        constraintMainForecast.visibility = View.VISIBLE
    }

    override fun getWeatherError(error: String) {

        showToast(error)
    }

    override fun onClickItem(forecast: Forecast) {
        showToast("Previsão para ${Utils.timestampToDate(forecast.dt.toLong())}")
    }

    override fun showProgress(type: BaseView.ProgressType) {

        onLoadingStart()
    }

    override fun hideProgress() {

        onLoadingFinish()
    }

    @SuppressLint("MissingPermission")
    override fun onConnected(p0: Bundle?) {
        mUserLocation = LocationServices.FusedLocationApi.getLastLocation(mGoogleApiClient)
        initData()
    }

    override fun onConnectionSuspended(p0: Int) {

    }

    override fun onConnectionFailed(p0: ConnectionResult) {

        Log.e("Erro Connection", p0.errorMessage)
    }
}
