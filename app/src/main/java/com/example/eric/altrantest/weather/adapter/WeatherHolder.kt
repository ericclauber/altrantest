package com.example.eric.altrantest.weather.adapter

import android.content.Context
import android.support.v7.widget.RecyclerView
import android.view.View
import com.example.eric.altrantest.R
import com.example.eric.altrantest.base.LineItem
import com.example.eric.altrantest.base.Utils
import com.example.eric.altrantest.webservice.weather.Forecast
import kotlinx.android.synthetic.main.line_item_forecast.view.*

/**
 * Created by eric on 21/06/18.
 */
class WeatherHolder(var mContext: Context, itemView: View,
                    var mListener: WeatherAdapter.OnClickListener) : RecyclerView.ViewHolder(itemView) {

    fun bindItem(position: Int, item: LineItem) {

        val forecast = item.obj as Forecast

        itemView.textViewWeatherMain.text = forecast.weather?.get(0)?.main
        itemView.textViewDate.text = Utils.timestampToDate(forecast.dt.toLong())

        val maxTemp = Utils.convertKelvinToCelcius(forecast.temp?.max!!)
        val minTemp = Utils.convertKelvinToCelcius(forecast.temp?.min)
        itemView.textViewMaxMinTemperature.text = "$maxTemp / $minTemp°C"


        when (forecast.weather?.get(0)?.main) {
            "Clear" -> {
                itemView.imageViewIcon.setImageResource(R.drawable.sun)
            }
            "Clouds" -> {
                itemView.imageViewIcon.setImageResource(R.drawable.cloud)
            }
            "Rain" -> {
                itemView.imageViewIcon.setImageResource(R.drawable.rain)
            }
        }

        itemView.constraintMainForecastItem.setOnClickListener { mListener.onClickItem(forecast) }
    }
}