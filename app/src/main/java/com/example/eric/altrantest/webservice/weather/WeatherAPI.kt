package com.example.eric.altrantest.webservice.weather

import com.example.eric.altrantest.base.Constants
import retrofit2.Call
import retrofit2.http.*

/**
 * Created by eric on 20/06/18.
 */
interface WeatherAPI {

    @GET("forecast/daily?")
    fun getForecast(@Query("lat") latitude: Float,
                    @Query("lon") longitude: Float,
                    @Query("cnt") quantDays: Int,
                    @Query("appid")  APIKEY :String): Call<WeatherResponse>
}