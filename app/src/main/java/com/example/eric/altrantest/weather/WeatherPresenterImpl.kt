package com.example.eric.altrantest.weather

import android.content.Context
import com.example.eric.altrantest.base.BaseActivity
import com.example.eric.altrantest.base.BaseView
import com.example.eric.altrantest.webservice.weather.WeatherResponse
import kotlin.properties.Delegates

/**
 * Created by eric on 20/06/18.
 */
class WeatherPresenterImpl : WeatherPresenter, WeatherListener {

    var context: Context by Delegates.notNull()
    var view: WeatherView by Delegates.notNull()
    var weatherInteractorImpl: WeatherInteractorImpl by Delegates.notNull()

    constructor(context: Context, view: WeatherView) {

        this.context = context
        this.view = view

        view.showProgress(BaseView.ProgressType.PROGRESS_DIALOG)

        weatherInteractorImpl = WeatherInteractorImpl()
    }

    override fun getWeather(latitude: Float, longitude: Float, quantDays: Int) {

        weatherInteractorImpl.getWeather(context, this, latitude, longitude, quantDays)
    }

    override fun getWeatherSuccess(weatherResponse: WeatherResponse) {
        (context as BaseActivity).runOnUiThread {

            view.getWeatherSuccess(weatherResponse)
            view.hideProgress()
        }
    }

    override fun getWeatherError(error: String) {
        (context as BaseActivity).runOnUiThread {

            view.getWeatherError(error)
            view.hideProgress()
        }
    }
}

interface WeatherPresenter {

    fun getWeather(latitude: Float,
                   longitude: Float,
                   quantDays: Int)
}