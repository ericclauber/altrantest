package com.example.eric.altrantest.webservice.weather

data class City(val id: Int,
                val name: String,
                val coord: Coordinates,
                val country: String,
                val population: Int)