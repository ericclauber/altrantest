package com.example.eric.altrantest.login

import android.content.ContentValues
import android.content.Intent
import android.os.Bundle
import com.example.eric.altrantest.R
import com.example.eric.altrantest.base.BaseActivity
import com.example.eric.altrantest.base.Utils
import com.example.eric.altrantest.databasehelper.DbHelper
import com.example.eric.altrantest.weather.WeatherActivity
import com.example.eric.altrantest.webservice.login.User
import kotlinx.android.synthetic.main.activity_login.*
import kotlinx.android.synthetic.main.custom_header_bar.*

class LoginActivity : BaseActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_login)

        initUI()
    }

    fun initUI() {

        textViewTitleHeaderBar.text = getString(R.string.login_title)
        buttonEnter.setOnClickListener { if (isValidForm()) saveUserAndStartForecast() }
    }

    fun initData() {}

    fun isValidForm(): Boolean {

        var isValidForm = true
        val isValidEmail = Utils.isValidEmail(editTextEmail.text.toString())

        if (editTextName.text.isEmpty()
                || editTextEmail.text.isEmpty()
                || editTextPassword.text.isEmpty()) {

            showSnackbar(getString(R.string.all_fields_empty_error_login))
            isValidForm = false
        }

        if (!isValidEmail) {

            textInputEmail.error = getString(R.string.email_login_error)
            isValidForm = false
        }

        return isValidForm
    }

    fun saveUserAndStartForecast() {

        val dbHelper = DbHelper(this, User.UserTable.SQL_CREATE_USER, User.UserTable.TABLE_NAME)
        val saveUser = dbHelper.writableDatabase

        val values = ContentValues().apply {

            put(User.UserTable.COLUMN_NAME_NAME_USER, editTextName.text.toString())
            put(User.UserTable.COLUMN_NAME_EMAIL, editTextEmail.text.toString())
            put(User.UserTable.COLUMN_NAME_PASSWORD, editTextPassword.text.toString())
        }

        saveUser.insert(User.UserTable.TABLE_NAME, null, values)
        dbHelper.close()

        startActivity(Intent(this, WeatherActivity::class.java))
    }
}
