package com.example.eric.altrantest.weather

import android.content.Context
import com.example.eric.altrantest.R
import com.example.eric.altrantest.base.Constants
import com.example.eric.altrantest.base.enums.ServerStatusEnum
import com.example.eric.altrantest.base.webservice.BaseInteractorImpl
import com.example.eric.altrantest.webservice.weather.WeatherAPI
import com.example.eric.altrantest.webservice.weather.WeatherResponse
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

/**
 * Created by eric on 20/06/18.
 */
class WeatherInteractorImpl : BaseInteractorImpl(), WeatherInteractor {
    override fun getWeather(context: Context, weatherListener: WeatherListener, latitude: Float, longitude: Float, quantDays: Int) {
        val api = createAPI(Constants.URL_BASE, WeatherAPI::class.java) as WeatherAPI
        val call = api.getForecast(latitude, longitude, quantDays, Constants.APIKEY)

        call.enqueue(object : Callback<WeatherResponse> {
            override fun onResponse(call: Call<WeatherResponse>?, response: Response<WeatherResponse>?) {
                if (response?.code() == ServerStatusEnum.STATUS_OK.code) {

                    var weatherResponse = response?.body()

                    weatherListener.getWeatherSuccess(weatherResponse)
                    return
                }

                if (response?.code() == ServerStatusEnum.STATUS_FORBIDDEN.code
                        || response?.code() == ServerStatusEnum.STATUS_NOT_FOUND.code) {

                    weatherListener.getWeatherError(response.message())
                }
                var errorMessage = ""

                try {
                    var error = convetJsonToObjct(response?.errorBody().toString(), Error::class.java) as Error

                    if (error != null && error.message != null) {

                        errorMessage = error.message!!
                    } else {
                        errorMessage = response!!.message()
                    }
                } catch (e: Exception) {

                }
                weatherListener.getWeatherError(errorMessage)
            }

            override fun onFailure(call: Call<WeatherResponse>?, t: Throwable?) {
                if (t?.message != null) {

                    weatherListener.getWeatherError(t.message.toString())
                } else {

                    weatherListener.getWeatherError("")
                }
            }
        })
    }
}

interface WeatherInteractor {

    fun getWeather(context: Context,
                   weatherListener: WeatherListener,
                   latitude: Float,
                   longitude: Float,
                   quantDays: Int)
}