package com.example.eric.altrantest.webservice.weather

data class WeatherResponse(val city: City,
                           val cod: Int,
                           val message: Float,
                           val cnt: Int,
                           val list: List<Forecast>)