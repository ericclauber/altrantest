package com.example.eric.altrantest.webservice.login

import android.provider.BaseColumns

/**
 * Created by eric on 19/06/18.
 */
class User(val name: String, val email: String, val password: String) {

    object UserTable : BaseColumns {

        const val TABLE_NAME = "user"
        const val COLUMN_NAME_NAME_USER = "name"
        const val COLUMN_NAME_EMAIL = "email"
        const val COLUMN_NAME_PASSWORD = "password"

        const val SQL_CREATE_USER = "$TABLE_NAME (${BaseColumns._ID} INTEGER PRIMARY KEY," +
                "$COLUMN_NAME_NAME_USER TEXT," +
                "$COLUMN_NAME_EMAIL TEXT," +
                "$COLUMN_NAME_PASSWORD TEXT)"
    }
}