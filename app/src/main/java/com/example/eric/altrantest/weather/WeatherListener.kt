package com.example.eric.altrantest.weather

import com.example.eric.altrantest.webservice.weather.WeatherResponse

/**
 * Created by eric on 20/06/18.
 */
interface WeatherListener {

    fun getWeatherSuccess(weatherResponse : WeatherResponse)
    fun getWeatherError(error: String)
}