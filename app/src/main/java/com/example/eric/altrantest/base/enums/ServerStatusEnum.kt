package com.example.eric.altrantest.base.enums

/**
 * Created by eric on 20/06/18.
 */
enum class ServerStatusEnum private constructor(code: Int) {

    STATUS_OK(200),
    STATUS_CREATED(201),
    STATUS_DELETE_OK(204),
    STATUS_UNAUTHORIZED(401),
    STATUS_FORBIDDEN(403),
    STATUS_NOT_FOUND(404),
    STATUS_NOT_ACCEPTABLE(406),
    STATUS_LAST_PAGE(500),
    STATUS_INVALID_RESPONSE(502);

    var code: Int = 0
        internal set

    init {
        this.code = code
    }

}