package com.example.eric.altrantest.webservice.weather

data class Coordinates(val lon: Double, val lat: Double)