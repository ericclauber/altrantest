package com.example.eric.altrantest.base

import android.Manifest
import android.app.AlertDialog
import android.app.ProgressDialog
import android.content.pm.PackageManager
import android.os.Looper
import android.support.design.widget.Snackbar
import android.support.v4.app.ActivityCompat
import android.support.v4.app.Fragment
import android.support.v4.content.ContextCompat
import android.support.v7.app.AppCompatActivity
import android.view.View
import android.widget.Toast
import com.example.eric.altrantest.R
import java.util.*

/**
 * Created by eric on 19/06/18.
 */
open class BaseActivity : AppCompatActivity() {

    var progressDialog: ProgressDialog? = null
    open val PERMISSION_REQUEST_LOCATION = 2

    fun onLoadingStart() {
        onLoadingFinish()

        progressDialog = ProgressDialog(this@BaseActivity, AlertDialog.THEME_DEVICE_DEFAULT_LIGHT)
        progressDialog!!.setMessage(getString(R.string.text_wait_progress_dialog))
        progressDialog!!.setCancelable(false)

        try {
            progressDialog!!.show()
        } catch (e: Exception) {
            e.printStackTrace()
        }
    }

    fun onLoadingFinish() {

        if (progressDialog != null) {

            progressDialog?.dismiss()
            progressDialog = null
        }
    }

    fun showToast(message: String?) {

        Toast.makeText(this, message, Toast.LENGTH_SHORT).show()
    }

    fun showSnackbar(txt: String) {
        val parentLayout = window.decorView.findViewById<View>(android.R.id.content)
        Snackbar.make(parentLayout, txt, Snackbar.LENGTH_LONG)
                .setAction(getString(R.string.ok_snack_bar)) { }
                .show()
    }


    override fun onBackPressed() {
        super.onBackPressed()
    }

    fun checkLocationPermission(): Boolean {
        val result = ContextCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION)
        return result == PackageManager.PERMISSION_GRANTED
    }

    fun requestLocationPermission() {
        if (ActivityCompat.shouldShowRequestPermissionRationale(this, Manifest.permission.ACCESS_FINE_LOCATION)) {
            ActivityCompat.requestPermissions(this, arrayOf(Manifest.permission.ACCESS_FINE_LOCATION), PERMISSION_REQUEST_LOCATION)
        } else {
            ActivityCompat.requestPermissions(this, arrayOf(Manifest.permission.ACCESS_FINE_LOCATION), PERMISSION_REQUEST_LOCATION)
        }
    }
}