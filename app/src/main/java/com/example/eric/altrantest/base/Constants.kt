package com.example.eric.altrantest.base

import com.example.eric.altrantest.BuildConfig

/**
 * Created by eric on 20/06/18.
 */
object Constants {

    const val APIKEY = BuildConfig.APIKEY
    val URL_BASE = BuildConfig.URL_BASE
}