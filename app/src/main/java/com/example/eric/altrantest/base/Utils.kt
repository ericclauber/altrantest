package com.example.eric.altrantest.base

import java.text.SimpleDateFormat
import java.util.*

/**
 * Created by eric on 20/06/18.
 */
object Utils {

    val TEMPERATURE_KELVIN_BASE = 273.15f
    val HUNDRED_FOR_CALC_TIMESTAMP = 1000

    fun convertKelvinToCelcius(temperature: Float): String {

        return (Math.round(temperature - TEMPERATURE_KELVIN_BASE)).toString()
    }

    fun timestampToDate(timestamp : Long): String {

        val date = Date()
        date.time = timestamp

        val sdf = SimpleDateFormat("dd/MM/yyyy", Locale("pt", "BR"))
        val formatedDate = sdf.format(date.time * HUNDRED_FOR_CALC_TIMESTAMP)

        return formatedDate
    }

    fun isValidEmail(email : String) : Boolean{

        return android.util.Patterns.EMAIL_ADDRESS.matcher(email).matches()
    }
}