package com.example.eric.altrantest

import android.util.Log
import com.example.eric.altrantest.base.Utils
import kotlinx.android.synthetic.main.activity_login.*
import org.junit.Test

import org.junit.Assert.*
import java.text.SimpleDateFormat
import java.util.*

/**
 * Example local unit test, which will execute on the development machine (host).
 *
 * See [testing documentation](http://d.android.com/tools/testing).
 */
class ExampleUnitTest {
    @Test
    fun addition_isCorrect() {
        assertEquals(4, 2 + 2)
    }

    @Test
    fun isValidForm() {

        var isValidForm = true

        if ("qualquer".isEmpty()) {

            isValidForm = false
        }

        assertEquals(true, isValidForm)
    }

    @Test
    fun isCorrectTeperatureConvert() {

        assertEquals("0.0", Utils.convertKelvinToCelcius(273.15f))
    }

    @Test
    fun timestampToDate() {

        val date = Date()
        date.time = 1529506800

        val sdf = SimpleDateFormat("dd/MM/YYY HH:mm",Locale("pt","BR"))
        val dateFormat = sdf.format(date.time * 1000)

        assertEquals("20/06/2018 12:00", dateFormat)
    }
}
